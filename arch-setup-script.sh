#!/bin/bash

# run to do some everytime stuff in my setup

# git clone https://gitlab.org/jodd/ass

# USERNAME="jan"
# FULLNAME="Jan Berg Michelsen"
# EMAIL="jan.berg.michelsen@gmail.com"
# HOSTNAME="vin"
# 
#
pacman -S curl dialog inetutils lvm2 mtools netctl nvme-cli reflector rsync sysfsutils device-mapper xz zstd

pacman -S bash-completion cdrtools devtools diffutils encfs exfatprogs f2fs-tools fatresize
pacman -S fsarchiver fuse3 gnome-disk-utility gnome-keyring

pacman -S hwdate hddetect hwinfo intel-ucode


